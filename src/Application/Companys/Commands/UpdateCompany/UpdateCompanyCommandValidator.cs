﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using project2.Application.Common.Interfaces;
using project2.Application.Companys.Commands.CreateCompany;
using Microsoft.EntityFrameworkCore;

namespace project2.Application.Companys.Commands.UpdateCompany;
public class UpdateCompanyCommandValidator : AbstractValidator<CreateCompanyCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdateCompanyCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(v => v.Name)
            .NotEmpty().WithMessage("Name is required.")
            .MaximumLength(200).WithMessage("Name must not exceed 200 characters.")
            .Matches(@"^[a-zA-Z0-9_ ]*$").WithMessage("Name cannot contain special characters.");

        RuleFor(v => v.Email)
            .NotEmpty().WithMessage("Email is required.")
            .MaximumLength(200).WithMessage("Email must not exceed 200 characters.")
            .EmailAddress().WithMessage("Email must be a valid email address.")
            .Matches(@"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$").WithMessage("Email cannot contain special characters or white spaces.")
            .MustAsync(BeUniqueEmail).WithMessage("The specified email already exists.");

        RuleFor(v => v.Phone)
            .NotEmpty().WithMessage("Phone is required.")
            .MaximumLength(20).WithMessage("Phone must not exceed 20 characters.")
            .Matches(@"^[0-9]{10}$").WithMessage("Phone must be a 10-digit number.")
            .MustAsync(BeUniquePhone).WithMessage("The specified phone number already exists.")
            .Must(phone => phone.IndexOf(' ') < 0).WithMessage("Phone cannot contain white spaces.");
    }

    public async Task<bool> BeUniqueEmail(string email, CancellationToken cancellationToken)
    {
        return await _context.Companies
            .AllAsync(c => c.Email != email, cancellationToken);
    }

    public async Task<bool> BeUniquePhone(string phone, CancellationToken cancellationToken)
    {
        return await _context.Companies
            .AllAsync(c => c.Phone != phone, cancellationToken);
    }
}

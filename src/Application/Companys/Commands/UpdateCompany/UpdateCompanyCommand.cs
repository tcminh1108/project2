﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using project2.Application.Common.Exceptions;
using project2.Application.Common.Interfaces;
using project2.Domain.Entities;
using MediatR;

namespace project2.Application.Companys.Commands.UpdateCompany;
public record UpdateCompanyCommand : IRequest
{
    public int Id { get; init; }
    public string? Name { get; init; }
    public string? Phone { get; init; }
    public string? Email { get; init; }
    public string? Address { get; init; }
}

public class UpdateCompanyCommandHandler : IRequestHandler<UpdateCompanyCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdateCompanyCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Unit> Handle(UpdateCompanyCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.Companies
            .FindAsync(new object[] { request.Id }, cancellationToken);

        if (entity == null)
        {
            throw new NotFoundException(nameof(Company), request.Id);
        }

        if (request.Name != null)
        {
            entity.Name = request.Name;
        }

        if (request.Phone != null)
        {
            entity.Phone = request.Phone;
        }

        if (request.Email != null)
        {
            entity.Email = request.Email;
        }

        if (request.Address != null)
        {
            entity.Address = request.Address;
        }
        entity.LastModified = DateTime.Now;
        entity.LastModifiedBy = "Admin";
        await _context.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using project2.Application.Common.Interfaces;
using project2.Domain.Entities;

namespace project2.Application.Companys.Commands.CreateCompany;
public record CreateCompanyCommand : IRequest<int>
{
    public string Name { get; init; }
    public string Phone { get; init; }
    public string Email { get; init; }
    public string Address { get; init; }
}
public class CreateCompanyCommandHandler : IRequestHandler<CreateCompanyCommand, int>
{
    private readonly IApplicationDbContext _context;

    public CreateCompanyCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<int> Handle(CreateCompanyCommand request, CancellationToken cancellationToken)
    {
        var entity = new Company();

        entity.Name = request.Name;
        entity.Phone = request.Phone;
        entity.Email = request.Email;
        entity.Address = request.Address;
        entity.Created = DateTime.Now;
        entity.CreatedBy = "Admin";
        entity.LastModified = DateTime.Now;
        entity.LastModifiedBy = "Admin";

        _context.Companies.Add(entity);

        await _context.SaveChangesAsync(cancellationToken);

        return entity.Id;
    }
}


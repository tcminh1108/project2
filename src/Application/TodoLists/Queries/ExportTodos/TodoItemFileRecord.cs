﻿using project2.Application.Common.Mappings;
using project2.Domain.Entities;

namespace project2.Application.TodoLists.Queries.ExportTodos;

public class TodoItemRecord : IMapFrom<TodoItem>
{
    public string? Title { get; set; }

    public bool Done { get; set; }
}

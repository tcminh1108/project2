﻿using project2.Application.TodoLists.Queries.ExportTodos;

namespace project2.Application.Common.Interfaces;

public interface ICsvFileBuilder
{
    byte[] BuildTodoItemsFile(IEnumerable<TodoItemRecord> records);
}

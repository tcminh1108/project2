﻿using project2.Application.Common.Interfaces;

namespace project2.Infrastructure.Services;

public class DateTimeService : IDateTime
{
    public DateTime Now => DateTime.Now;
}

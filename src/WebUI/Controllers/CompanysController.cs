﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using project2.Application.Companys.Commands.CreateCompany;
using project2.Application.Companys.Commands.DeleteCompany;
using project2.Application.Companys.Commands.UpdateCompany;
using project2.Application.Companys.Queries.GetCompanies;
using project2.WebUI.Controllers;

namespace WebUI.Controllers;

public class CompanysController : ApiControllerBase
{
    [HttpGet]
    public async Task<ActionResult<List<CompanyDto>>> GetCompaniesWithPagination([FromQuery] GetCompaniesQuery query)
    {
        return await Mediator.Send(query);
    }

    [HttpPost]
    public async Task<ActionResult<int>> Create(CreateCompanyCommand command)
    {
        return await Mediator.Send(command);
    }

    [HttpPut("{id}")]
    public async Task<ActionResult> Update(int id, UpdateCompanyCommand command)
    {
        if (id != command.Id)
        {
            return BadRequest();
        }

        await Mediator.Send(command);

        return NoContent();
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult> Delete(int id)
    {
        await Mediator.Send(new DeleteCompanyCommand(id));

        return NoContent();
    }
}

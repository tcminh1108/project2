﻿global using project2.Domain.Common;
global using project2.Domain.Entities;
global using project2.Domain.Enums;
global using project2.Domain.Events;
global using project2.Domain.Exceptions;
global using project2.Domain.ValueObjects;